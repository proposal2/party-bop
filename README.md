# Party Bop

Party Bop is an app that creates music playlists based on the disired energy level of the party.
This idea stems from Roman parties where someone had the job of balancing the wine/water ratios to maintain the best paty atmosphere.

## Detail

The creator inputs parameters from a questionnaire including:
- Genres of music
- Themes of the party
- Nostalgia songs

Then the creatro is asked for the start and end times of the party and using a histogram defines the energy level over time.

The software uses these inputs to pick songs that sync together well and match the energy of the party at the point in time.
